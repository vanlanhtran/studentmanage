/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doankii.In_Menu;

import static LOGIN.LOGIN.locale;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import java.util.ResourceBundle;
/**
 *
 * @author Administrator
 */
public class InDanhSachSv extends javax.swing.JInternalFrame {

    /**
     * Creates new form InDanhSachSv
     */
    public InDanhSachSv() {
        initComponents();
        chuyenngu();
        showAllStd();
        addClass();
        
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        jLabel1.setText(bundle.getString("key99"));
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key14"));
        head1.add(bundle.getString("key73"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
        jTable1.setModel(new DefaultTableModel(data1, head1)); 
        lbLop.setText(bundle.getString("key14"));
        btXuatDS.setText(bundle.getString("key75"));
    }
    public void showAllStd(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key14"));
        head1.add(bundle.getString("key73"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="	select S.StdId,S.StdName,C.ClassName,c.Course,S.StdAddress,S.StdGender,s.StdEmail,s.StdPhone from Students as S\n" +
                "	  join Class as C on S.ClassId = C.ClassId  ";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("StdName"));
                    cus1.addElement(rs.getString("ClassName"));
                    cus1.addElement(rs.getString("Course"));
                    cus1.addElement(rs.getString("StdAddress"));
                    cus1.addElement(rs.getString("StdGender"));
                    cus1.addElement(rs.getString("StdEmail"));
                    cus1.addElement(rs.getString("Stdphone"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
                     System.out.println("sdsdsd");
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }    
    }
    public void addClass(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        jComboBox1.addItem(bundle.getString("key76"));
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den database
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }   

            Statement stm = con.createStatement();
            String sql= "select Classname from class";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
             
            
        while(rs.next()){   
            jComboBox1.addItem(rs.getString("ClassName"));
            System.out.println(rs.getString("ClassName"));
        }
            
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        btXuatDS = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        lbLop = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jComboBox1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboBox1MouseClicked(evt);
            }
        });
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        btXuatDS.setText("Xuất Danh Sách");
        btXuatDS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btXuatDSActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        lbLop.setText("Lớp:");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Grade Management For HKL Training Center");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 579, Short.MAX_VALUE)
                        .addComponent(lbLop)
                        .addGap(51, 51, 51)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86)
                        .addComponent(btXuatDS)
                        .addGap(373, 373, 373))
                    .addComponent(jScrollPane1)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 723, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(234, 234, 234))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLop)
                    .addComponent(btXuatDS))
                .addGap(35, 35, 35)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBox1MouseClicked
        // TODO add your handling code here:
           
    }//GEN-LAST:event_jComboBox1MouseClicked

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        String strClassName = jComboBox1.getSelectedItem().toString();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        int nbCbb = jComboBox1.getSelectedIndex();
       Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key14"));
        head1.add(bundle.getString("key73"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
        if(nbCbb == 0){
            showAllStd();
        }else{
                try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
           String sql ="select S.StdId,S.StdName,C.ClassName,c.Course,S.StdAddress,S.StdGender,s.StdEmail,s.StdPhone from Students as S "
                   + "join Class as C on S.ClassId = C.ClassId\n" +
                    "	where ClassName = '"+strClassName+"'";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("StdName"));
                    cus1.addElement(rs.getString("ClassName"));
                    cus1.addElement(rs.getString("Course"));
                    cus1.addElement(rs.getString("StdAddress"));
                    cus1.addElement(rs.getString("StdGender"));
                    cus1.addElement(rs.getString("StdEmail"));
                    cus1.addElement(rs.getString("Stdphone"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        }
         
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void btXuatDSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btXuatDSActionPerformed
        // TODO add your handling code here:
        int nbCbb = jComboBox1.getSelectedIndex();
        if(nbCbb == 0){
                    try {
                    //2. Nap driver tuong ung voi database tren SQL Server:
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                    //3. Ket noi den database
                    String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                            + "databaseName=Doan1123new5;user=sa;password=123456";
                    Connection con = DriverManager.getConnection(connectionUrl);

                    if (con != null) {
                        System.out.println("Connection is successful.");
                    } else {
                        System.out.println("Connection is failed. Please try again.");
                    }                

                    JasperReport report = JasperCompileManager.compileReport("src/doankii/In_Menu/InDanhSachSv_All.jrxml");
                     JasperPrint print = JasperFillManager.fillReport(report,null,con);
                     JasperViewer.viewReport(print);

                    } catch(Exception e){
                        e.printStackTrace();
                    }
        }else{
                try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den database
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }                
                String TenLop = jComboBox1.getSelectedItem().toString();
                HashMap param = new HashMap();
                param.put("tenlop", TenLop);
                JasperReport report = JasperCompileManager.compileReport("src/doankii/In_Menu/InDanhSachSv_Lop1.jrxml");
                 JasperPrint print = JasperFillManager.fillReport(report,param,con);
                 JasperViewer.viewReport(print);

            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_btXuatDSActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btXuatDS;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lbLop;
    // End of variables declaration//GEN-END:variables
}
