/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doankii.In_Menu;

import static LOGIN.LOGIN.locale;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import java.util.ResourceBundle;
/**
 *
 * @author Administrator
 */
public class InKQHTSv extends javax.swing.JInternalFrame {

    /**
     * Creates new form InKQHTSV_All
     */
    public InKQHTSv() {
        initComponents();
        addClass();
        chuyenngu();
        showKQHTAllStd();
        
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        jLabel2.setText(bundle.getString("key99"));
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key14"));
        head1.add(bundle.getString("key73"));
        head1.add(bundle.getString("key7"));
        head1.add(bundle.getString("key8"));
        jTable1.setModel(new DefaultTableModel(data1, head1)); 
        jLabel1.setText(bundle.getString("key14"));
        jButton1.setText(bundle.getString("key77"));
    }
    public void addClass(){
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        jComboBox1.addItem(bundle.getString("key76"));
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den database
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }   

            Statement stm = con.createStatement();
            String sql= "select Classname from class";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
             
            
        while(rs.next()){   
            jComboBox1.addItem(rs.getString("ClassName"));
            System.out.println(rs.getString("ClassName"));
        }
            
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void showKQHTAllStd(){
       
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key14"));
        head1.add(bundle.getString("key73"));
        head1.add(bundle.getString("key7"));
        head1.add(bundle.getString("key8"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from C8TongKetAll";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("StdName"));
                    cus1.addElement(rs.getString("ClassName"));
                    cus1.addElement(rs.getString("Course"));
                    cus1.addElement(rs.getString("Trung Binh"));
                    cus1.addElement(rs.getString("xeploai"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jButton1.setText("Xuất Bảng Điểm");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel1.setText("jLabel1");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setText("Grade Management For HKL Training Center");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(497, 497, 497)
                .addComponent(jLabel1)
                .addGap(66, 66, 66)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(95, 95, 95)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(341, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 753, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(255, 255, 255))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel2)
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        int nbCbb = jComboBox1.getSelectedIndex();
        String strClassName =jComboBox1.getSelectedItem().toString();
        if(nbCbb == 0){
            showKQHTAllStd();
        }else{
            Vector datanull = new Vector();
            Vector data1 = new Vector();
            Vector head1  = new Vector();
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key14"));
        head1.add(bundle.getString("key73"));
        head1.add(bundle.getString("key7"));
        head1.add(bundle.getString("key8"));

            try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den selectdatabase
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }


                // hien thi du lieu tren table:
                //1. get dât from table in db --> co dc rs : chua all data của bảng
                Statement stm = con.createStatement();
              String sql ="select * from C8TongKetAll where ClassName = '"+strClassName+"'";
               jTable1.setModel(new DefaultTableModel(datanull, head1)); 
                ResultSet rs = stm.executeQuery(sql);//import pk resultset
                // xoa du lieu truoc khi tim :
                //jTable2.setModel(new DefaultTableModel(datanull, head1));
                while(rs.next()){     
                        Vector cus1 = new Vector();
                        cus1.addElement(rs.getString("StdId"));
                        cus1.addElement(rs.getString("StdName"));
                        cus1.addElement(rs.getString("ClassName"));
                        cus1.addElement(rs.getString("Course"));
                        cus1.addElement(rs.getString("Trung Binh"));
                        cus1.addElement(rs.getString("xeploai"));
                        data1.add(cus1);
                         jTable1.setModel(new DefaultTableModel(data1, head1));
                }       

                con.close();

            } catch (ClassNotFoundException | SQLException ex) {
                ex.printStackTrace();
            } 
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int nbCbb = jComboBox1.getSelectedIndex();
        String strClassName =jComboBox1.getSelectedItem().toString();
        if(nbCbb == 0){
             try {
                    //2. Nap driver tuong ung voi database tren SQL Server:
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                    //3. Ket noi den database
                    String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                            + "databaseName=Doan1123new5;user=sa;password=123456";
                    Connection con = DriverManager.getConnection(connectionUrl);

                    if (con != null) {
                        System.out.println("Connection is successful.");
                    } else {
                        System.out.println("Connection is failed. Please try again.");
                    }                

                    JasperReport report = JasperCompileManager.compileReport("src/doankii/In_Menu/InKQHTSv_All.jrxml");
                     JasperPrint print = JasperFillManager.fillReport(report,null,con);
                     JasperViewer.viewReport(print);

                    } catch(Exception e){
                        e.printStackTrace();
                    }
        }else{
                try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den database
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }                
                String TenLop = jComboBox1.getSelectedItem().toString();
                HashMap param = new HashMap();
                param.put("tenlop", TenLop);
                JasperReport report = JasperCompileManager.compileReport("src/doankii/In_Menu/InKQHTSv_Std.jrxml");
                 JasperPrint print = JasperFillManager.fillReport(report,param,con);
                 JasperViewer.viewReport(print);

            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
