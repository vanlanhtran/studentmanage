/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doanhkii.NhapMoiDuLieu;

import static LOGIN.LOGIN.locale;
import static doanhkii.NhapMoiDuLieu.frmStudent_Main.PATTERN_EMAIL;
import static doanhkii.NhapMoiDuLieu.frmStudent_Main.PATTERN_PHONE;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.ResourceBundle;
/**
 *
 * @author PC
 */
public class frmTeacher_Main extends javax.swing.JInternalFrame {
public static final String PATTERN_TEACHER = "^[T][c][h]\\d+$";
    /**
     * Creates new form frmTeacher_Main
     */
    public frmTeacher_Main() {
        initComponents();
        showTeacherOnTable();
        chuyenngu();
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        lbThongTinGv.setText(bundle.getString("key66"));
        lbMaGv.setText(bundle.getString("key56"));
        lbMaGv2.setText(bundle.getString("key56"));
        lbDiaChi.setText(bundle.getString("key45"));
        lbGioiTinh.setText(bundle.getString("key46"));
        lbNgaySinh.setText(bundle.getString("key44"));
        lbTenGv.setText(bundle.getString("key13"));
        lbSDt.setText(bundle.getString("key49"));
        btTim.setText(bundle.getString("key38"));
        btHienThiAll.setText(bundle.getString("key39"));
        btCapNhat.setText(bundle.getString("key40"));
        btThemMoi.setText(bundle.getString("key50"));
        btXoa.setText(bundle.getString("key51"));
        rdNam.setText(bundle.getString("key47"));
        rdNu.setText(bundle.getString("key48"));
    }
    public void showTeacherOnTable(){
        
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key56")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key46"));
        head1.add(bundle.getString("key45"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Teachers";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("TchId"));
                    cus1.addElement(rs.getString("TchName"));
                    cus1.addElement(rs.getString("TchBirthday"));
                    cus1.addElement(rs.getString("TchGender"));
                    cus1.addElement(rs.getString("TchAddress"));
                    cus1.addElement(rs.getString("TchEmail"));
                    cus1.addElement(rs.getString("TchPhone"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
    }
    public void showTeacherOnTableTheoId(){
          ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String tchId = tchSearchId.getText();
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key56")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key46"));
        head1.add(bundle.getString("key45"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Teachers where TchId = '"+tchId+"'";
          
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            
             if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                jTable1.setModel(new DefaultTableModel(datanull, head1));
                do{     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("TchId"));
                    cus1.addElement(rs.getString("TchName"));
                    cus1.addElement(rs.getString("TchBirthday"));
                    cus1.addElement(rs.getString("TchGender"));
                    cus1.addElement(rs.getString("TchAddress"));
                    cus1.addElement(rs.getString("TchEmail"));
                    cus1.addElement(rs.getString("TchPhone"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
                } while(rs.next());      
            }else{
                 resetAll();
                 showTeacherOnTable();
                JOptionPane.showMessageDialog(this, bundle.getString("key57"));
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void resetAll(){
        jTextField1.setText("");
        jTextField2.setText("");
        jTextField4.setText("");
        jTextField6.setText("");
        jTextField5.setText("");
        jDateChooser1.setDate(null);
        tchSearchId.setText("");
    }
    public boolean testCase(){
        boolean test = true;
        String tchName = jTextField2.getText();
        String tchId = jTextField1.getText();
        String tchAddress = jTextField5.getText();
        String tchEmail = jTextField6.getText();
        String tchPhone = jTextField4.getText();
        //check ngày lớn hơn : 
        Date date = new Date();
        Date dateChoose = jDateChooser1.getDate();
        
        if((tchName.equals("")) | (tchName.equals("")) | (jDateChooser1.getDate() == null) |
           (date.compareTo(dateChoose) != 1) |
           (tchAddress.equals("")) | (!(rdNam.isSelected()) & !(rdNu.isSelected())) |
           !(tchEmail.matches(PATTERN_EMAIL)) | !(tchPhone.matches(PATTERN_PHONE))  | !(tchId.matches(PATTERN_TEACHER))    )
        {
            test = false;
        } 
        return test;
    }
    public  boolean testCaseAddTrungIdMailPhone(){ // email id phone khi add phải khác vs e,i,p trong dbase
        boolean test = true;
        String tchId = jTextField1.getText();
        String tchEmail = jTextField6.getText();
        String tchPhone = jTextField4.getText();
        try {
               //2. Nap driver tuong ung voi database tren SQL Server:
               Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

               //3. Ket noi den database
               String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                       + "databaseName=Doan1123new5;user=sa;password=123456";
               Connection con = DriverManager.getConnection(connectionUrl);

               if (con != null) {
                   System.out.println("Connection is successful.");
               } else {
                   System.out.println("Connection is failed. Please try again.");
               }   

               Statement stm = con.createStatement();
               // kiem tra xem co trung` id | phone | email
               String sql= "select * from Teachers where TchEmail = '"+tchEmail+"' or TchPhone = '"+tchPhone+"' or TchId = '"+tchId+"'";
               ResultSet rs = stm.executeQuery(sql);//import pk resultset
               // xoa du lieu truoc khi tim :


           if(rs.next()){   
               test = false;
           }
               con.close();
           } catch (ClassNotFoundException | SQLException ex) {
               ex.printStackTrace();
           }
        
        return test;
    }
    public boolean testCaseUpdateTrungIdMailPhone(){ //email và phone khác với sv hiện k đc chọn
        boolean test = true;
        String tchId = jTextField1.getText();
        String tchEmail = jTextField6.getText();
        String tchPhone = jTextField4.getText();
         try {
               //2. Nap driver tuong ung voi database tren SQL Server:
               Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

               //3. Ket noi den database
               String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                       + "databaseName=Doan1123new5;user=sa;password=123456";
               Connection con = DriverManager.getConnection(connectionUrl);

               if (con != null) {
                   System.out.println("Connection is successful.");
               } else {
                   System.out.println("Connection is failed. Please try again.");
               }   

               Statement stm = con.createStatement();
               // kiem tra xem co trung` id | phone | email
               String sql= "select * from Teachers where (TchEmail = '"+tchEmail+"' or TchPhone = '"+tchPhone+"') and TchId != '"+tchId+"'";
               ResultSet rs = stm.executeQuery(sql);//import pk resultset
               // xoa du lieu truoc khi tim :


           if(rs.next()){   
               test = false;
           }
               con.close();
           } catch (ClassNotFoundException | SQLException ex) {
               ex.printStackTrace();
           }
        
        return test;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        lbMaGv = new javax.swing.JLabel();
        lbTenGv = new javax.swing.JLabel();
        lbNgaySinh = new javax.swing.JLabel();
        lbGioiTinh = new javax.swing.JLabel();
        lbDiaChi = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        rdNam = new javax.swing.JRadioButton();
        rdNu = new javax.swing.JRadioButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        lbSDt = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        btCapNhat = new javax.swing.JButton();
        btXoa = new javax.swing.JButton();
        btTim = new javax.swing.JButton();
        btThemMoi = new javax.swing.JButton();
        btHienThiAll = new javax.swing.JButton();
        lbMaGv2 = new javax.swing.JLabel();
        tchSearchId = new javax.swing.JTextField();
        lbThongTinGv = new javax.swing.JLabel();

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Mã Giáo Viên", "Tên Giáo Viên", "Ngày Sinh", "Giới Tính", "Địa Chỉ", "Email"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbMaGv.setText("Mã Giáo Viên");

        lbTenGv.setText("Tên Giáo Viên");

        lbNgaySinh.setText("Ngày Sinh");

        lbGioiTinh.setText("Giới Tính");

        lbDiaChi.setText("Địa Chỉ");

        jLabel11.setText("Email");

        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdNam);
        rdNam.setText("Nam");

        buttonGroup1.add(rdNu);
        rdNu.setText("Nữ");

        lbSDt.setText("Số điện thoại");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lbSDt))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbMaGv)
                            .addComponent(lbTenGv)
                            .addComponent(lbNgaySinh))
                        .addGap(60, 60, 60)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 555, Short.MAX_VALUE)
                                .addComponent(lbGioiTinh))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbDiaChi)))))
                .addGap(66, 66, 66)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField5, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(rdNam)
                        .addGap(18, 18, 18)
                        .addComponent(rdNu))
                    .addComponent(jTextField4))
                .addGap(71, 71, 71))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbMaGv)
                            .addComponent(lbGioiTinh)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rdNam)
                            .addComponent(rdNu))
                        .addGap(38, 38, 38)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbTenGv)
                            .addComponent(lbDiaChi)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbNgaySinh))))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbSDt))
                .addGap(26, 26, 26))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btCapNhat.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\Update.png")); // NOI18N
        btCapNhat.setText("Cập Nhật");
        btCapNhat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCapNhatActionPerformed(evt);
            }
        });

        btXoa.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\delete.png")); // NOI18N
        btXoa.setText("     Xóa");
        btXoa.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btXoaActionPerformed(evt);
            }
        });

        btTim.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\search2.png")); // NOI18N
        btTim.setText("Tìm");
        btTim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btTimActionPerformed(evt);
            }
        });

        btThemMoi.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\add.png")); // NOI18N
        btThemMoi.setText("Thêm Mới");
        btThemMoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btThemMoiActionPerformed(evt);
            }
        });

        btHienThiAll.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\edit.png")); // NOI18N
        btHienThiAll.setText("Hiển thị tất cả");
        btHienThiAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHienThiAllActionPerformed(evt);
            }
        });

        lbMaGv2.setText("Mã giáo viên:");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbMaGv2)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(tchSearchId, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btThemMoi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btXoa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btCapNhat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btHienThiAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btTim, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(lbMaGv2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tchSearchId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btTim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(btHienThiAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btThemMoi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btCapNhat)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btXoa)
                .addGap(18, 18, 18))
        );

        lbThongTinGv.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbThongTinGv.setForeground(new java.awt.Color(0, 51, 204));
        lbThongTinGv.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbThongTinGv.setText("Thông Tin Giáo Viên");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbThongTinGv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbThongTinGv)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1347, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 512, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed

    private void btXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btXoaActionPerformed
        // TODO add your handling code here:
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String tchId = jTextField1.getText();
        String tchName = jTextField2.getText();
         int b = JOptionPane.showConfirmDialog(this, 
                bundle.getString("key58"),
                "",JOptionPane.YES_NO_OPTION);
        if(b==0){
                try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den database
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }                

                //4. Tao va thuc hien cau lenh SQL:
                Statement stm = con.createStatement();           

                String sql = "exec xoaTeacher '"+tchId+"' ";
                stm.executeUpdate(sql); 
                con.close();
                JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                resetAll();
                showTeacherOnTable();
                } catch (ClassNotFoundException | SQLException ex) {
                    ex.printStackTrace();
                }
        }else{
            resetAll();
        }
    }//GEN-LAST:event_btXoaActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
         int a = jTable1.getSelectedRow();
        String tchId = jTable1.getValueAt(a, 0).toString();
        jTextField1.setText(tchId);
        
        String tchName = jTable1.getValueAt(a, 1).toString();
         jTextField2.setText(tchName);
        
        
        
        String tchBirthday = jTable1.getValueAt(a, 2).toString();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = formatter.parse(tchBirthday);
            jDateChooser1.setDate(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        
        String tchAdd = jTable1.getValueAt(a, 4).toString();
        jTextField5.setText(tchAdd);
        
        String tchGender = jTable1.getValueAt(a, 3).toString();
        if(tchGender.equals("Nam") | tchGender.equals("nam")){
            rdNam.setSelected(true);
            rdNu.setSelected(false);
        }else{
            rdNu.setSelected(true);
            rdNam.setSelected(false);
        }
        
        String tchEmail = jTable1.getValueAt(a, 5).toString();
        jTextField6.setText(tchEmail);
        String tchPhone = jTable1.getValueAt(a, 6).toString();
        jTextField4.setText(tchPhone);
    }//GEN-LAST:event_jTable1MouseClicked

    private void btTimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btTimActionPerformed
        // TODO add your handling code here:
   
        showTeacherOnTableTheoId();
    }//GEN-LAST:event_btTimActionPerformed

    private void btHienThiAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHienThiAllActionPerformed
        // TODO add your handling code here:
        showTeacherOnTable();
        resetAll();
    }//GEN-LAST:event_btHienThiAllActionPerformed

    private void btThemMoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btThemMoiActionPerformed
        // TODO add your handling code here:
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key56")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key46"));
        head1.add(bundle.getString("key45"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       String tchName = jTextField2.getText();
       String tchId = jTextField1.getText();
        Date date = jDateChooser1.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String tchDate = formatter.format(date);
        String tchAddress = jTextField5.getText();
        String tchGender = "";
        if(rdNam.isSelected()){
            tchGender = "Nam";
        }else if(rdNu.isSelected()){
            tchGender = "Nữ";
        }
        String tchEmail = jTextField6.getText();
        String tchPhone = jTextField4.getText();
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
           Statement stm = con.createStatement();
            if(!(testCase())){
                JOptionPane.showMessageDialog(this,bundle.getString("key59"));
            }
            if (!(testCaseAddTrungIdMailPhone())){
                JOptionPane.showMessageDialog(this, bundle.getString("key60"));
            }
            if(testCase() & testCaseAddTrungIdMailPhone()){
                  String sql ="insert into Teachers(TchId,TchName,TchBirthday ,TchGender,TchAddress,TchEmail,TchPhone) \n" +
                  "values ('"+tchId+"',N'"+tchName+"','"+tchDate+"',N'"+tchGender+"',N'"+tchAddress+"','"+tchEmail+"','"+tchPhone+"')";

                   stm.executeUpdate(sql);//import pk resultset
                   showTeacherOnTable();
                   JOptionPane.showMessageDialog(this,bundle.getString("key42"));
                   resetAll();
            }
            
            con.close();  
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        } 
    }//GEN-LAST:event_btThemMoiActionPerformed

    private void btCapNhatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCapNhatActionPerformed
        // TODO add your handling code here:
        String strSearchId = tchSearchId.getText();
        Vector data1 = new Vector();
        Vector head1 = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key56")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key46"));
        head1.add(bundle.getString("key45"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       String tchName = jTextField2.getText();
       String tchId = jTextField1.getText();
        Date date = jDateChooser1.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String tchDate = formatter.format(date);
        String tchAddress = jTextField5.getText();
        String tchGender = "";
        if(rdNam.isSelected()){
            tchGender = "Nam";
        }else if(rdNu.isSelected()){
            tchGender = "Nữ";
        }
        String tchEmail = jTextField6.getText();
        String tchPhone = jTextField4.getText();
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            
            
             if(!(testCase())){
                JOptionPane.showMessageDialog(this,bundle.getString("key59"));
            }
            if (!(testCaseUpdateTrungIdMailPhone())){
                JOptionPane.showMessageDialog(this, bundle.getString("key60")); 
            }
            if(testCase() & testCaseUpdateTrungIdMailPhone())
            {
                  String sql ="update Teachers set TchName = N'"+tchName+"' ,"
                          + " TchBirthday = '"+tchDate+"',TchGender=N'"+tchGender+"',TchAddress = N'"+tchAddress+"',"
                          + "TchEmail ='"+tchEmail+"',TchPhone='"+tchPhone+"'\n" +
                        "where TchId = '"+tchId+"'";

                   stm.executeUpdate(sql);//import pk resultset
                   showTeacherOnTable();
                   JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                   resetAll();
            }
            con.close();  
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        } 
    }//GEN-LAST:event_btCapNhatActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCapNhat;
    private javax.swing.JButton btHienThiAll;
    private javax.swing.JButton btThemMoi;
    private javax.swing.JButton btTim;
    private javax.swing.JButton btXoa;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JLabel lbDiaChi;
    private javax.swing.JLabel lbGioiTinh;
    private javax.swing.JLabel lbMaGv;
    private javax.swing.JLabel lbMaGv2;
    private javax.swing.JLabel lbNgaySinh;
    private javax.swing.JLabel lbSDt;
    private javax.swing.JLabel lbTenGv;
    private javax.swing.JLabel lbThongTinGv;
    private javax.swing.JRadioButton rdNam;
    private javax.swing.JRadioButton rdNu;
    private javax.swing.JTextField tchSearchId;
    // End of variables declaration//GEN-END:variables
}
