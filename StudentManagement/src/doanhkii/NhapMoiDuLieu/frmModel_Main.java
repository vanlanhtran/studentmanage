/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doanhkii.NhapMoiDuLieu;

import LOGIN.TeacherLOGIN;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static LOGIN.LOGIN.locale;

/**
 *
 * @author PC
 */
public class frmModel_Main extends javax.swing.JInternalFrame {

    /**
     * Creates new form frmModel_Main
     */
    public frmModel_Main() {
        initComponents();
        showModuleTrenTable();
        chuyenngu();
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        
        mm.setText(bundle.getString("key206"));
        lbTTmonhoc.setText(bundle.getString("key205"));
        lbHocki.setText(bundle.getString("key208"));
        lbMamon.setText(bundle.getString("key206"));
        lbMaGv.setText(bundle.getString("key212"));
        lbMota.setText(bundle.getString("key209"));
        lbSogio.setText(bundle.getString("key210"));
        lbTKB.setText(bundle.getString("key211"));
        lbTenmon.setText(bundle.getString("key207"));
        btTim.setText(bundle.getString("key214"));
        btAdd.setText(bundle.getString("key216"));
        btCapnhat.setText(bundle.getString("key218"));
        btXoa.setText(bundle.getString("key217"));
        btdisplayAll.setText(bundle.getString("key215"));
    }
    
    public boolean testnumber(){
    boolean test =true;
    try{
        int i = Integer.parseInt(txt3.getText());
        test = false;
    }catch(Exception e){
        test = true;
    }
    return test;
}
    public void resetAll(){
        txt1.setText("");
        txt2.setText("");
        txt3.setText("");
        jTextArea1.setText("");
        txt5.setText("");
        txt6.setText("");
        jComboBox1.getSelectedItem();
    }
    public void showModuleTrenTable(){
        
        String txtString = this.txt1.getText();
         Vector data1 = new Vector();
        Vector head1  = new Vector();
        Vector datanull = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key206")); 
        head1.add(bundle.getString("key207"));
        head1.add(bundle.getString("key208"));
        head1.add(bundle.getString("key209"));
        head1.add(bundle.getString("key210"));
        head1.add(bundle.getString("key211"));
        head1.add(bundle.getString("key212"));
        
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Modules";
            
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            
            
            if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                jTable1.setModel(new DefaultTableModel(datanull, head1));
                do{     
                        Vector cus1 = new Vector();
                        cus1.addElement(rs.getString("ModId"));
                        cus1.addElement(rs.getString("ModName"));
                        cus1.addElement(rs.getString("Semester"));
                        cus1.addElement(rs.getString("ModDesc"));
                        cus1.addElement(rs.getString("ModHours"));
                        cus1.addElement(rs.getString("Schedule"));
                        cus1.addElement(rs.getString("TchId"));
//                        cus1.addElement(rs.getString("Stdphone"));
                        data1.add(cus1);
                         jTable1.setModel(new DefaultTableModel(data1, head1));
                } while(rs.next());      
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void showModuleTrenTabletheoID(){
        String txttimmon = this.txttimmon.getText();
         Vector data1 = new Vector();
        Vector head1  = new Vector();
        Vector datanull = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key206")); 
        head1.add(bundle.getString("key207"));
        head1.add(bundle.getString("key208"));
        head1.add(bundle.getString("key209"));
        head1.add(bundle.getString("key210"));
        head1.add(bundle.getString("key211"));
        head1.add(bundle.getString("key212"));
     
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Modules where ModId = '"+txttimmon+"'";
            
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            
            
            if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                jTable1.setModel(new DefaultTableModel(datanull, head1));
                do{     
                        Vector cus1 = new Vector();
                        cus1.addElement(rs.getString("ModId"));
                        cus1.addElement(rs.getString("ModName"));
                        cus1.addElement(rs.getString("Semester"));
                        cus1.addElement(rs.getString("ModDesc"));
                        cus1.addElement(rs.getString("ModHours"));
                        cus1.addElement(rs.getString("Schedule"));
                        cus1.addElement(rs.getString("TchId"));
//                        cus1.addElement(rs.getString("Stdphone"));
                        data1.add(cus1);
                         jTable1.setModel(new DefaultTableModel(data1, head1));
                } while(rs.next());      
            }else{
                JOptionPane.showMessageDialog(this,bundle.getString("key122"));
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        lbTTmonhoc = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        lbMamon = new javax.swing.JLabel();
        lbTenmon = new javax.swing.JLabel();
        lbHocki = new javax.swing.JLabel();
        lbMaGv = new javax.swing.JLabel();
        lbMota = new javax.swing.JLabel();
        txt5 = new javax.swing.JTextField();
        txt1 = new javax.swing.JTextField();
        txt2 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        lbSogio = new javax.swing.JLabel();
        txt3 = new javax.swing.JTextField();
        lbTKB = new javax.swing.JLabel();
        txt6 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel9 = new javax.swing.JPanel();
        btCapnhat = new javax.swing.JButton();
        btXoa = new javax.swing.JButton();
        btAdd = new javax.swing.JButton();
        mm = new javax.swing.JLabel();
        txttimmon = new javax.swing.JTextField();
        btTim = new javax.swing.JButton();
        btdisplayAll = new javax.swing.JButton();

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã Môn Học", "Tên Môn Học", "Học Kỳ", "Mô Tả", "Số Giờ Học", "Lịch Dạy", "Mã Giáo Viên"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        lbTTmonhoc.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbTTmonhoc.setForeground(new java.awt.Color(0, 51, 204));
        lbTTmonhoc.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTTmonhoc.setText("Thông Tin Môn Học");

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbMamon.setText("Mã Môn Học");

        lbTenmon.setText("Tên Môn Học");

        lbHocki.setText("Học Kỳ");

        lbMaGv.setText("Mã giáo viên");

        lbMota.setText("Mô Tả");

        txt5.setText(" ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", " " }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        lbSogio.setText("Số giờ học");

        txt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt3ActionPerformed(evt);
            }
        });

        lbTKB.setText("Lịch dạy");

        txt6.setText(" ");
        txt6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt6ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbMamon)
                    .addComponent(lbTenmon)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lbMota))
                    .addComponent(lbHocki))
                .addGap(70, 70, 70)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(txt1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 403, Short.MAX_VALUE)
                                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lbSogio, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(lbTKB, javax.swing.GroupLayout.Alignment.TRAILING)))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(lbMaGv)))
                                .addGap(70, 70, 70))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(txt2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txt5, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt3, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txt6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(130, 130, 130))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMamon)
                    .addComponent(txt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbSogio, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbTKB)
                            .addComponent(txt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbTenmon))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbHocki))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMaGv)
                    .addComponent(txt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbMota)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btCapnhat.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\Update.png")); // NOI18N
        btCapnhat.setText("Cập Nhật");
        btCapnhat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCapnhatActionPerformed(evt);
            }
        });

        btXoa.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\delete.png")); // NOI18N
        btXoa.setText("      Xóa");
        btXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btXoaActionPerformed(evt);
            }
        });

        btAdd.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\add.png")); // NOI18N
        btAdd.setText("Thêm Mới");
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddActionPerformed(evt);
            }
        });

        mm.setText("Mã Môn Học");

        btTim.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\search2.png")); // NOI18N
        btTim.setText("Tìm");
        btTim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btTimActionPerformed(evt);
            }
        });

        btdisplayAll.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\edit.png")); // NOI18N
        btdisplayAll.setText("Hiển Thị Tất Cả");
        btdisplayAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btdisplayAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttimmon, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(mm)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btXoa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btCapnhat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btdisplayAll, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addComponent(btTim, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttimmon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btTim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btdisplayAll, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(btCapnhat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btXoa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbTTmonhoc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 47, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(lbTTmonhoc, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btXoaActionPerformed
        // TODO add your handling code here:
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String strMamonhoc = txt1.getText();
        int b = JOptionPane.showConfirmDialog(this, 
                bundle.getString("key144"),
                "",JOptionPane.YES_NO_OPTION);
        if(b == 0){
                try {
                 //2. Nap driver tuong ung voi database tren SQL Server:
                 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                 //3. Ket noi den database
                 String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                         + "databaseName=Doan1123new5;user=sa;password=123456";
                 Connection con = DriverManager.getConnection(connectionUrl);

                 if (con != null) {
                     System.out.println("Connection is successful.");
                 } else {
                     System.out.println("Connection is failed. Please try again.");
                 }                

                 //4. Tao va thuc hien cau lenh SQL:
                 Statement stm = con.createStatement();           

                 String sql = "exec XoaModId '"+strMamonhoc+"' ";
                 stm.executeUpdate(sql); 
                 con.close();
                 JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                 resetAll();
                 showModuleTrenTable();
             } catch (ClassNotFoundException | SQLException ex) {
                 ex.printStackTrace();
             } 
        }
        
        
    }//GEN-LAST:event_btXoaActionPerformed

     public boolean kiemtraAdd(){
         boolean test = true;
        String strModId = txt1.getText();
        String strName= txt2.getText();
//        String strhocky(co du lieu ko can check)
        String strmota = jTextArea1.getText();
        String strsogiohoc = txt3.getText();
        String strlichday = txt6.getText();
        String strmagiaovien = txt5.getText();
      
        if((strModId.equals("")) | (strName.equals("")) | strmota.equals("")| strsogiohoc.equals("")
                | strlichday.equals("")| strmagiaovien.equals("")
                )
        {
            test = false;
        } 
        return test;
}
     public boolean kiemtratrunglap(){
         boolean kiemtra = false;
         String strModId = txt1.getText();
         String strName = txt2.getText();
         try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Modules where ModId = '"+strModId+"'or ModName='"+strName+"'";
            
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            
            
            if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                kiemtra = true;
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
         return kiemtra;
     }
     public boolean kiemtraGiaoVien(){
         boolean kiemtramagiaovien = false;
         String strmagiaovien = txt5.getText();
         try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Teachers where TchId= '"+strmagiaovien+"'";
            
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            
            
            if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                kiemtramagiaovien = true;
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
         return kiemtramagiaovien;
     }
     public boolean kiemtraupdate (){
         boolean kiemtrupdate1 = false;
         String strModName = txt2.getText();
         String strModId = txt1.getText();
         try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Modules where ModName= '"+strModName+"'and ModId !='"+strModId+"'";
            
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            
            
            if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                kiemtrupdate1 = true;
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
         return kiemtrupdate1;
         
         
     }
    private void btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddActionPerformed
        // TODO add your handling code here:
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key206")); 
        head1.add(bundle.getString("key207"));
        head1.add(bundle.getString("key208"));
        head1.add(bundle.getString("key209"));
        head1.add(bundle.getString("key210"));
        head1.add(bundle.getString("key211"));
        head1.add(bundle.getString("key212"));
        String strModId = txt1.getText();
        String strName= txt2.getText();
//        String strhocky(co du lieu ko can check)
        String strhocky = jComboBox1.getSelectedItem().toString();
        String strmota = jTextArea1.getText();
        String strsogiohoc = txt3.getText();
        String strlichday = txt6.getText();
        String strmagiaovien = txt5.getText();
        //gọi hàm kiểm tra null
        
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            if(!kiemtraGiaoVien()){
                JOptionPane.showMessageDialog(this, bundle.getString("key430"));
            }
            if(!kiemtraAdd() | testnumber()){
            JOptionPane.showMessageDialog(this,bundle.getString("key123"));
        }
        if(kiemtratrunglap()){
            JOptionPane.showMessageDialog(this,bundle.getString("key124"));
        }
        if(kiemtraAdd()& !kiemtratrunglap() & !testnumber() & kiemtraGiaoVien()){
                  String sql ="insert into Modules(ModId,ModName,Semester,ModDesc,ModHours,Schedule,TchId) \n" +
"values ('"+strModId+"',N'"+strName+"','"+strhocky+"',N'"+strmota+"',N'"+strsogiohoc+"',N'"+strlichday+"','"+strmagiaovien+"')";
                   stm.executeUpdate(sql);//import pk resultset
                   showModuleTrenTable();
                   JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                   resetAll();
            }
            
            con.close();  
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        } 
        
    }//GEN-LAST:event_btAddActionPerformed
        
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
         int a = jTable1.getSelectedRow();
        String strId = jTable1.getValueAt(a, 0).toString();
        txt1.setText(strId);
        
        String strName = jTable1.getValueAt(a, 1).toString();
         txt2.setText(strName);
         
         String strhocky = jTable1.getValueAt(a, 2).toString();
        jComboBox1.setSelectedItem(strhocky);
        
         String strmota = jTable1.getValueAt(a, 3).toString();
        jTextArea1.setText(strmota);
        
        String strsogiohoc = jTable1.getValueAt(a, 4).toString();
        txt3.setText(strsogiohoc);
        
        String strlichday = jTable1.getValueAt(a, 5).toString();
        txt6.setText(strlichday);
   
        String strmagiaovien = jTable1.getValueAt(a, 6 ).toString();
        txt5.setText(strmagiaovien);

               
    }//GEN-LAST:event_jTable1MouseClicked

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void btTimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btTimActionPerformed
        // TODO add your handling code here:
         String strModId = txttimmon.getText();
        showModuleTrenTabletheoID();
        
    }//GEN-LAST:event_btTimActionPerformed

    private void btdisplayAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btdisplayAllActionPerformed
        // TODO add your handling code here:
          showModuleTrenTable();
        resetAll();
    }//GEN-LAST:event_btdisplayAllActionPerformed

    private void txt6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt6ActionPerformed

    private void btCapnhatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCapnhatActionPerformed
        // TODO add your handling code here:
         Vector data1 = new Vector();
        Vector head1  = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key206")); 
        head1.add(bundle.getString("key207"));
        head1.add(bundle.getString("key208"));
        head1.add(bundle.getString("key209"));
        head1.add(bundle.getString("key210"));
        head1.add(bundle.getString("key211"));
        head1.add(bundle.getString("key212"));
        String strModId = txt1.getText();
        String strName= txt2.getText();
//        String strhocky(co du lieu ko can check)
        String strhocky = jComboBox1.getSelectedItem().toString();
        String strmota = jTextArea1.getText();
        String strsogiohoc = txt3.getText();
        String strlichday = txt6.getText();
        String strmagiaovien = txt5.getText();
       try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            if(!kiemtraGiaoVien()){
                JOptionPane.showMessageDialog(this, bundle.getString("key430"));
            }
            if(!kiemtraAdd()| testnumber()){
            JOptionPane.showMessageDialog(this,bundle.getString("key123"));
        }
        if(kiemtraupdate()){
            JOptionPane.showMessageDialog(this,bundle.getString("key125"));
        }
            if(kiemtraAdd()& !kiemtraupdate() & !testnumber() & kiemtraGiaoVien()){
                  String sql =" update Modules set ModName='"+strName+"', Semester='"+strhocky+"', "
                          + "ModDesc= N'"+strmota+"', ModHours='"+strsogiohoc+"', Schedule='"+strlichday+"',"
                          + "TchId='"+strmagiaovien+"'where ModId='"+strModId+"'";
                   stm.executeUpdate(sql);//import pk resultset
                   showModuleTrenTable();
                   JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                   resetAll();
            }
            
            con.close();  
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        } 
    }//GEN-LAST:event_btCapnhatActionPerformed

    private void txt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt3ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txt3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btCapnhat;
    private javax.swing.JButton btTim;
    private javax.swing.JButton btXoa;
    private javax.swing.JButton btdisplayAll;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lbHocki;
    private javax.swing.JLabel lbMaGv;
    private javax.swing.JLabel lbMamon;
    private javax.swing.JLabel lbMota;
    private javax.swing.JLabel lbSogio;
    private javax.swing.JLabel lbTKB;
    private javax.swing.JLabel lbTTmonhoc;
    private javax.swing.JLabel lbTenmon;
    private javax.swing.JLabel mm;
    private javax.swing.JTextField txt1;
    private javax.swing.JTextField txt2;
    private javax.swing.JTextField txt3;
    private javax.swing.JTextField txt5;
    private javax.swing.JTextField txt6;
    private javax.swing.JTextField txttimmon;
    // End of variables declaration//GEN-END:variables
}
