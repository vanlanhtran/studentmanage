/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doanhkii.NhapMoiDuLieu;

import static LOGIN.LOGIN.locale;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.table.DefaultTableModel;
import java.util.ResourceBundle;
/**
 *
 * @author PC
 */
public class frmMark_Main extends javax.swing.JInternalFrame {
       public static String STDID = "^[S][t][d]\\d+$";
     public static   String MODID = "^[M][d]\\d+$";
    /**
     * Creates new form frmMark_Main
     */
    public frmMark_Main() {
        initComponents();
        showMarksOnTable();
       chuyenngu();
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        lbTTDiemHv.setText(bundle.getString("key61"));
        lbMaSv.setText(bundle.getString("key32"));
        lbMaMon.setText(bundle.getString("key33"));
        lbMaMon2.setText(bundle.getString("key33"));
        lbMaBangDiem.setText(bundle.getString("key31"));
        lbDiemLT.setText(bundle.getString("key36"));
        lbDiemTH.setText(bundle.getString("key37"));
        btTim.setText(bundle.getString("key38"));
        btHienThiAll.setText(bundle.getString("key39"));
        btCapNhat.setText(bundle.getString("key40"));
        btThemMoi.setText(bundle.getString("key50"));
        btXoa.setText(bundle.getString("key51"));
        
    }
    public void showMarksOnTable(){
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key31")); 
        head1.add(bundle.getString("key32"));
        head1.add(bundle.getString("key33"));
        head1.add(bundle.getString("key37"));
        head1.add(bundle.getString("key36"));
        head1.add(bundle.getString("key34"));
        head1.add(bundle.getString("key35"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Marks";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("MarkId"));
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("ModId"));
                    cus1.addElement(rs.getString("PracticeMark"));
                    cus1.addElement(rs.getString("TheoryMark"));
                    cus1.addElement(rs.getString("pass"));
                    cus1.addElement(rs.getString("average"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }     
    }
    public void showMarksOnTableTheoId(){
        String strSearch = txtSearch.getText();
         Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        head1.add(bundle.getString("key31")); 
        head1.add(bundle.getString("key32"));
        head1.add(bundle.getString("key33"));
        head1.add(bundle.getString("key37"));
        head1.add(bundle.getString("key36"));
        head1.add(bundle.getString("key34"));
        head1.add(bundle.getString("key35"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Marks where ModId = '"+strSearch+"'";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
         
          if(rs.next()){
              do{     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("MarkId"));
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("ModId"));
                    cus1.addElement(rs.getString("PracticeMark"));
                    cus1.addElement(rs.getString("TheoryMark"));
                    cus1.addElement(rs.getString("pass"));
                    cus1.addElement(rs.getString("average"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            } while(rs.next());    
          } else{
              JOptionPane.showMessageDialog(this, bundle.getString("key27"));
          }
           resetAll();
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
    }
    public void resetAll(){
        txtPracti.setText("");
        txtMaSv.setText("");
        txtMaMonHoc.setText("");
        txtTheoRy.setText("");
        jLabel4.setText("");
        txtSearch.setText("");
    }
    public boolean testCaseNull(){
        boolean test = true;
        String strStdId = txtMaSv.getText();
        String strModId = txtMaMonHoc.getText();
        String strTheoMark = txtTheoRy.getText();
        String strPracMark = txtPracti.getText();
        if((strStdId.equals("")) | (strModId.equals("")) | (strTheoMark.equals("")) | (strPracMark.equals(""))){
            test = false;
            System.out.println("Bị Null");
        }
        return test;
    }
    public boolean testCaseCoMaHayKhong(){

        boolean test = false;
        String strStdId = txtMaSv.getText();
        String strModId = txtMaMonHoc.getText();
       String strMaSv = txtMaSv.getText();
        String strMaMon = txtMaMonHoc.getText();
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            Statement stm2 = con.createStatement();
          String sql ="select * from Students where StdId = '"+strMaSv+"'";
          String sql2 = "select * from Modules where modid = '"+strMaMon+"'";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            ResultSet rs2 = stm2.executeQuery(sql2);
            if(rs.next() & rs2.next()  & strMaSv.matches(STDID) & strMaMon.matches(MODID)){      
//                & strMaSv.matches(STDID) & strMaMon.matches(MODID)
                    System.out.println("CÓ kq ");
                    test = true;
            }else{
                System.out.println("Không có kq");
            }              
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
        return test;
    }
    public boolean testCaseMarkNumber(){
        boolean test = false;
        try{
            float a = Float.parseFloat(txtTheoRy.getText());
            float b = Float.parseFloat(txtPracti.getText());
                if(((a >= 0) & (a <= 10)) & ((b >= 0) & (b <= 10))){
                    test = true;
                }
        }catch(Exception ex){
            
            System.out.println("K phai so");
        }
        return test;
    }
    public boolean testCaseMaCoTrungKhongChoUpDate(){
        boolean test = true;
        String strMaSv = txtMaSv.getText();
        String strMaMon = txtMaMonHoc.getText();
        int nbMa = Integer.parseInt(jLabel4.getText());
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
          
          String sql ="select * from Marks where StdId = '"+strMaSv+"' and ModId = '"+strMaMon+"' and MarkId != '"+nbMa+"'";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset            
            if(rs.next()){     
                    
                    test = false;
            }else{
                System.out.println("Bạn nhập được");
            }              
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
        return test;
    }
    public boolean testCaseMaCoTrungKhongChoAdd(){
        boolean test = true;
        String strMaSv = txtMaSv.getText();
        String strMaMon = txtMaMonHoc.getText();
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
          
          String sql ="select * from Marks where StdId = '"+strMaSv+"' and ModId = '"+strMaMon+"'";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset            
            if(rs.next()){     
                    test = false;
            }else{
                System.out.println("Bạn nhập được");
            }              
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
        return test;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        lbTTDiemHv = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        lbMaSv = new javax.swing.JLabel();
        lbMaMon = new javax.swing.JLabel();
        lbDiemLT = new javax.swing.JLabel();
        txtMaSv = new javax.swing.JTextField();
        txtMaMonHoc = new javax.swing.JTextField();
        txtTheoRy = new javax.swing.JTextField();
        lbDiemTH = new javax.swing.JLabel();
        txtPracti = new javax.swing.JTextField();
        lbMaBangDiem = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btCapNhat = new javax.swing.JButton();
        btXoa = new javax.swing.JButton();
        btTim = new javax.swing.JButton();
        btThemMoi = new javax.swing.JButton();
        btHienThiAll = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        lbMaMon2 = new javax.swing.JLabel();

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbTTDiemHv.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbTTDiemHv.setForeground(new java.awt.Color(0, 51, 204));
        lbTTDiemHv.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTTDiemHv.setText("Thông Tin Điểm Học Viên");

        jTable1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã Bảng Điểm", "Mã Sinh Viên", "Mã Môn Học", "Điểm Thực Hành", "Điểm Lý Thuyết", "Qua Môn", "Trung Bình Môn"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbMaSv.setText("Mã Sinh Viên");

        lbMaMon.setText("Mã Môn Học");

        lbDiemLT.setText("Điểm Lý Thuyết");

        txtMaMonHoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaMonHocActionPerformed(evt);
            }
        });

        lbDiemTH.setText("Điểm Thực Hành");

        txtPracti.setText(" ");
        txtPracti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPractiActionPerformed(evt);
            }
        });

        lbMaBangDiem.setText("Mã Bảng Điểm");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbMaBangDiem)
                            .addComponent(lbMaSv))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtMaSv, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4)
                                    .addComponent(txtMaMonHoc, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(707, 707, 707)
                                .addComponent(lbDiemLT)))
                        .addGap(0, 4, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(lbMaMon)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbDiemTH)))
                .addGap(48, 48, 48)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTheoRy, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPracti, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(69, 69, 69))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbMaBangDiem)
                            .addComponent(jLabel4)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTheoRy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbDiemLT))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMaSv)
                    .addComponent(txtMaSv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbMaMon)
                            .addComponent(txtMaMonHoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPracti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbDiemTH))))
                .addGap(84, 84, 84))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btCapNhat.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\Update.png")); // NOI18N
        btCapNhat.setText("Cập Nhật");
        btCapNhat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCapNhatActionPerformed(evt);
            }
        });

        btXoa.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\delete.png")); // NOI18N
        btXoa.setText("Xóa");
        btXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btXoaActionPerformed(evt);
            }
        });

        btTim.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\search2.png")); // NOI18N
        btTim.setText("Tìm kiếm");
        btTim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btTimActionPerformed(evt);
            }
        });

        btThemMoi.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\add.png")); // NOI18N
        btThemMoi.setText("Thêm Mới");
        btThemMoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btThemMoiActionPerformed(evt);
            }
        });

        btHienThiAll.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\edit.png")); // NOI18N
        btHienThiAll.setText("Hiển Thị Tất Cả");
        btHienThiAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHienThiAllActionPerformed(evt);
            }
        });

        //try{
            //    NumberFormat x = NumberFormat.getInstance();
            //    txtPracti = new JFormattedTextField(x);
            //}catch(Exception ex){
            //    ex.printStackTrace();
            //}

        lbMaMon2.setText("Mã môn:");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(lbMaMon2)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btTim, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtSearch)
                    .addComponent(btHienThiAll, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btThemMoi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btCapNhat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btXoa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(24, 24, 24))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lbMaMon2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btTim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btHienThiAll)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btThemMoi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btCapNhat)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btXoa)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lbTTDiemHv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTTDiemHv)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1369, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(27, 27, 27)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 522, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(31, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btXoaActionPerformed
        // TODO add your handling code here:
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String strMaMon = txtMaMonHoc.getText();
        String strMaSv = txtMaSv.getText();
        int nbMa = Integer.parseInt(jLabel4.getText());
        int b = JOptionPane.showConfirmDialog(this, 
                bundle.getString("key62"),
                "",JOptionPane.YES_NO_OPTION);
        if(b==0){
                try {
                 //2. Nap driver tuong ung voi database tren SQL Server:
                 Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                 //3. Ket noi den selectdatabase
                 String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                         + "databaseName=Doan1123new5;user=sa;password=123456";
                 Connection con = DriverManager.getConnection(connectionUrl);

                 if (con != null) {
                     System.out.println("Connection is successful.");
                 } else {
                     System.out.println("Connection is failed. Please try again.");
                 }
                 Statement stm = con.createStatement();
                 String sql ="delete from Marks where MarkId = '"+nbMa+"'";           
                 stm.executeUpdate(sql);             
                 con.close();
                 JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                 resetAll();
                 showMarksOnTable();
             } catch (ClassNotFoundException | SQLException ex) {
                 ex.printStackTrace();
             }     
        }
    }//GEN-LAST:event_btXoaActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        int a = jTable1.getSelectedRow();    
        String strMaBangDiem = jTable1.getValueAt(a, 0).toString();
        jLabel4.setText(strMaBangDiem);
        String strId = jTable1.getValueAt(a, 1).toString();
         txtMaSv.setText(strId);   
        String strModId = jTable1.getValueAt(a, 2).toString();
        txtMaMonHoc.setText(strModId);
        String strTheoMark = jTable1.getValueAt(a, 4).toString();
        txtTheoRy.setText(strTheoMark);
        String strPracMark = jTable1.getValueAt(a, 3).toString();
        txtPracti.setText(strPracMark);
    

              
    }//GEN-LAST:event_jTable1MouseClicked

    private void btHienThiAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHienThiAllActionPerformed
        // TODO add your handling code here:
        showMarksOnTable();
        resetAll();
    }//GEN-LAST:event_btHienThiAllActionPerformed

    private void btTimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btTimActionPerformed
        // TODO add your handling code here:
        showMarksOnTableTheoId();
    }//GEN-LAST:event_btTimActionPerformed

    private void btThemMoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btThemMoiActionPerformed
        // TODO add your handling code here:
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String strMaSv = txtMaSv.getText();
        String strMaMon = txtMaMonHoc.getText();                  
         if(!testCaseNull() | !testCaseMarkNumber()){
            JOptionPane.showMessageDialog(this, bundle.getString("key63"));
        }if(!testCaseCoMaHayKhong()){
            JOptionPane.showMessageDialog(this,bundle.getString("key64"));
            
        }if(!testCaseMaCoTrungKhongChoAdd()){
            JOptionPane.showMessageDialog(this,bundle.getString("key65"));
        }
        if((testCaseNull())& (testCaseMarkNumber()) & (testCaseCoMaHayKhong()) & (testCaseMaCoTrungKhongChoAdd())){
            float nbTheoMark = Float.parseFloat(txtTheoRy.getText());
            float nbPracMark = Float.parseFloat(txtPracti.getText());
            float average = (nbPracMark + nbTheoMark)/2;
            String pass = "Có";
            if((nbPracMark < 5) | (nbTheoMark < 5)){
                pass = "Không";
            }
           
                try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den selectdatabase
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }
                Statement stm = con.createStatement();              
              String sql ="exec insertMark '"+strMaSv+"','"+strMaMon+"', '"+nbPracMark+"','"+nbTheoMark+"','"+pass+"','"+average+"'";
              
                stm.executeUpdate(sql);//import pk resultset
                JOptionPane.showMessageDialog(this,bundle.getString("key42"));
                showMarksOnTable();
                resetAll();
                con.close();

            } catch (ClassNotFoundException | SQLException ex) {
                ex.printStackTrace();
            }   
            
        }else{
            System.out.println("Ko add đc");
        }
        
            
        
    }//GEN-LAST:event_btThemMoiActionPerformed

    private void btCapNhatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCapNhatActionPerformed
        // TODO add your handling code here:
        String strMaSv = txtMaSv.getText();
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String strMaMon = txtMaMonHoc.getText();
        int maBangDiem = Integer.parseInt(jLabel4.getText());
        if(!testCaseNull() | !testCaseMarkNumber()){
            JOptionPane.showMessageDialog(this, bundle.getString("key63"));
        }if(!testCaseCoMaHayKhong()){
            JOptionPane.showMessageDialog(this, bundle.getString("key64"));
        }if(!testCaseMaCoTrungKhongChoUpDate()){
            JOptionPane.showMessageDialog(this,bundle.getString("key65"));
        }
        if((testCaseNull()) & (testCaseMarkNumber()) & (testCaseCoMaHayKhong()) & (testCaseMaCoTrungKhongChoUpDate())){
            float nbTheoMark = Float.parseFloat(txtTheoRy.getText());
            float nbPracMark = Float.parseFloat(txtPracti.getText());
            float average = (nbPracMark + nbTheoMark)/2;
            String pass = "Có";
            if((nbPracMark < 5) | (nbTheoMark < 5)){
                pass = "Không";
            }
             try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den selectdatabase
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }
                Statement stm = con.createStatement();              
              String sql ="exec updateMark  '"+maBangDiem+"','"+strMaSv+"','"+strMaMon+"',"
                      + " '"+nbPracMark+"','"+nbTheoMark+"','"+pass+"','"+average+"'";
              
                stm.executeUpdate(sql);//import pk resultset
                JOptionPane.showMessageDialog(this,bundle.getString("key42"));
                resetAll();
                showMarksOnTable();
                con.close();

            } catch (ClassNotFoundException | SQLException ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_btCapNhatActionPerformed

    private void txtMaMonHocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaMonHocActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaMonHocActionPerformed

    private void txtPractiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPractiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPractiActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCapNhat;
    private javax.swing.JButton btHienThiAll;
    private javax.swing.JButton btThemMoi;
    private javax.swing.JButton btTim;
    private javax.swing.JButton btXoa;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lbDiemLT;
    private javax.swing.JLabel lbDiemTH;
    private javax.swing.JLabel lbMaBangDiem;
    private javax.swing.JLabel lbMaMon;
    private javax.swing.JLabel lbMaMon2;
    private javax.swing.JLabel lbMaSv;
    private javax.swing.JLabel lbTTDiemHv;
    private javax.swing.JTextField txtMaMonHoc;
    private javax.swing.JTextField txtMaSv;
    private javax.swing.JTextField txtPracti;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtTheoRy;
    // End of variables declaration//GEN-END:variables
}
