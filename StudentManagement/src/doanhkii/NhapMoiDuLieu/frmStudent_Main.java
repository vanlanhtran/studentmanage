/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doanhkii.NhapMoiDuLieu;

import static LOGIN.LOGIN.locale;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.ResourceBundle;
/**
 *
 * @author PC
 */
public class frmStudent_Main extends javax.swing.JInternalFrame {
    // W : all ? , có thể là . - _ rồi tới \\@ (kí hiệu @) , sau đó 1 ký tự hoặc . ,rồi thêm 2 kí tự
    public static final String PATTERN_EMAIL = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    public static final String PATTERN_PHONE = "\\d{10}|\\d{11}"; // 10 số or (dùng | ) 11 số
    public static final String PATTERN_ID =  "^[S][t][d]\\d+$";
    /**
     * Creates new form frmStudent_Main
     */
    public frmStudent_Main() {
        initComponents();
        addClass();
        showStudent();
        chuyenngu();
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        lbTTinHocVien.setText(bundle.getString("key43"));
        lbMaSv.setText(bundle.getString("key12"));
        lbAddress.setText(bundle.getString("key45"));
        lbGender.setText(bundle.getString("key46"));
        lbHoTen.setText(bundle.getString("key13"));
        lbLop.setText(bundle.getString("key14"));
        lbMaSV2.setText(bundle.getString("key12"));
        lbNgaySinh.setText(bundle.getString("key44"));
        lbSDT.setText(bundle.getString("key49"));
        btCapNhat.setText(bundle.getString("key40"));
        btHienThiAll.setText(bundle.getString("key39"));
        btThemMoi.setText(bundle.getString("key50"));
        btTim.setText(bundle.getString("key38"));
        btXoa.setText(bundle.getString("key51"));
        rdNam.setText(bundle.getString("key47"));
        rdNu.setText(bundle.getString("key48"));
    }
    public void showStudent(){
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key52"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Students";
           jTable2.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("StdName"));
                    cus1.addElement(rs.getString("ClassId"));
                    cus1.addElement(rs.getString("StdBirthday"));
                    cus1.addElement(rs.getString("StdAddress"));
                    cus1.addElement(rs.getString("Stdgender"));
                    cus1.addElement(rs.getString("StdEmail"));
                    cus1.addElement(rs.getString("Stdphone"));
                    data1.add(cus1);
                     jTable2.setModel(new DefaultTableModel(data1, head1));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }     
    }
    public void showStudentTrenTableTheoId(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String txtSearchId = this.txtSearchId.getText();
         Vector data1 = new Vector();
        Vector head1  = new Vector();
        Vector datanull = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key52"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Students where StdId = '"+txtSearchId+"'";
            
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            
            
            if(rs.next()){
                // nếu có dl: xóa bảng hiện tại, thêm vào bảng mới
                jTable2.setModel(new DefaultTableModel(datanull, head1));
                do{     
                        Vector cus1 = new Vector();
                        cus1.addElement(rs.getString("StdId"));
                        cus1.addElement(rs.getString("StdName"));
                        cus1.addElement(rs.getString("ClassId"));
                        cus1.addElement(rs.getString("StdBirthday"));
                        cus1.addElement(rs.getString("StdAddress"));
                        cus1.addElement(rs.getString("Stdgender"));
                        cus1.addElement(rs.getString("StdEmail"));
                        cus1.addElement(rs.getString("Stdphone"));
                        data1.add(cus1);
                         jTable2.setModel(new DefaultTableModel(data1, head1));
                } while(rs.next());      
            }else{
                JOptionPane.showMessageDialog(this, bundle.getString("key26"));
                resetAll();
                showStudent();
            }
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void resetAll(){
        jTextField1.setText("");
        jTextField2.setText("");
        jTextField4.setText("");
        jTextField6.setText("");
        jTextField7.setText("");
        jDateChooser1.setDate(null);
        txtSearchId.setText("");
    }
    public void addClass(){
         try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den database
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }   

            Statement stm = con.createStatement();
            String sql= "select Classname from class";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
             
            
        while(rs.next()){   
            jComboBox1.addItem(rs.getString("ClassName"));
            System.out.println(rs.getString("ClassName"));
        }
            
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
    public boolean testCase(){//Null , email,phone, date ,
        boolean test = true;
        String strName = jTextField2.getText();
        String strId = jTextField1.getText();
        String strAddress = jTextField4.getText();
        String strEmail = jTextField6.getText();
        String strPhone = jTextField7.getText();
        //check ngày lớn hơn : 
        Date date = new Date();
        Date dateChoose = jDateChooser1.getDate();
        
        if((strName.equals(""))  |
           (strAddress.equals("")) | (!(rdNam.isSelected()) & !(rdNu.isSelected()))
                |!(strEmail.matches(PATTERN_EMAIL)) | !(strPhone.matches(PATTERN_PHONE))   | !(strId.matches(PATTERN_ID))
                | (jDateChooser1.getDate().toString() == null) | (date.compareTo(dateChoose) != 1)
                ) 
           
          // (date.compareTo(dateChoose) != 1)
            //
           // 
        {
            test = false;
        } 
        return test;
    }
    public boolean testCaseAddTrungIdMailPhone(){ // email id phone khi add phải khác vs e,i,p trong dbase
        boolean test = true;
        String strId = jTextField1.getText();
        String strEmail = jTextField6.getText();
        String strPhone = jTextField7.getText();
        try {
               //2. Nap driver tuong ung voi database tren SQL Server:
               Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

               //3. Ket noi den database
               String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                       + "databaseName=Doan1123new5;user=sa;password=123456";
               Connection con = DriverManager.getConnection(connectionUrl);

               if (con != null) {
                   System.out.println("Connection is successful.");
               } else {
                   System.out.println("Connection is failed. Please try again.");
               }   

               Statement stm = con.createStatement();
               // kiem tra xem co trung` id | phone | email
               String sql= "select * from students where StdEmail = '"+strEmail+"' or StdPhone = '"+strPhone+"' or StdId = '"+strId+"'";
               ResultSet rs = stm.executeQuery(sql);//import pk resultset
               // xoa du lieu truoc khi tim :


           if(rs.next()){

               test = false;
           }
               con.close();
           } catch (ClassNotFoundException | SQLException ex) {
               ex.printStackTrace();
           }
        
        return test;
    }
    public boolean testCaseUpdateTrungIdMailPhone(){ //email và phone khác với sv hiện k đc chọn
        boolean test = true;
        String strId = jTextField1.getText();
        String strEmail = jTextField6.getText();
        String strPhone = jTextField7.getText();
         try {
               //2. Nap driver tuong ung voi database tren SQL Server:
               Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

               //3. Ket noi den database
               String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                       + "databaseName=Doan1123new5;user=sa;password=123456";
               Connection con = DriverManager.getConnection(connectionUrl);

               if (con != null) {
                   System.out.println("Connection is successful.");
               } else {
                   System.out.println("Connection is failed. Please try again.");
               }   

               Statement stm = con.createStatement();
               // kiem tra xem co trung` id | phone | email
               String sql= "select * from students where (StdEmail = '"+strEmail+"' or StdPhone = '"+strPhone+"') and StdId != '"+strId+"'";
               ResultSet rs = stm.executeQuery(sql);//import pk resultset
               // xoa du lieu truoc khi tim :


           if(rs.next()){   
               test = false;
           }
               con.close();
           } catch (ClassNotFoundException | SQLException ex) {
               ex.printStackTrace();
           }
        
        return test;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel4 = new javax.swing.JPanel();
        lbTTinHocVien = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        lbMaSv = new javax.swing.JLabel();
        lbHoTen = new javax.swing.JLabel();
        lbLop = new javax.swing.JLabel();
        lbNgaySinh = new javax.swing.JLabel();
        lbAddress = new javax.swing.JLabel();
        lbGender = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lbSDT = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        rdNam = new javax.swing.JRadioButton();
        rdNu = new javax.swing.JRadioButton();
        jPanel6 = new javax.swing.JPanel();
        btCapNhat = new javax.swing.JButton();
        btXoa = new javax.swing.JButton();
        btTim = new javax.swing.JButton();
        btThemMoi = new javax.swing.JButton();
        lbMaSV2 = new javax.swing.JLabel();
        txtSearchId = new javax.swing.JTextField();
        btHienThiAll = new javax.swing.JButton();

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbTTinHocVien.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbTTinHocVien.setForeground(new java.awt.Color(0, 51, 204));
        lbTTinHocVien.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTTinHocVien.setText("Thông Tin Học Viên");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã Sinh Viên", "Họ Và Tên", "Mã Lớp", "Ngày Sinh", "Địa Chỉ", "Giới Tính", "Email", "Số Điện Thoại"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.setToolTipText("Thong Tin Sinh Vien");
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbMaSv.setText("Mã Sinh Viên");

        lbHoTen.setText("Họ Và Tên");

        lbLop.setText("Lớp");

        lbNgaySinh.setText("Ngày Sinh");

        lbAddress.setText("Địa Chỉ");

        lbGender.setText("Giới Tính");

        jLabel11.setText("Email");

        lbSDT.setText("Số Điện Thoại");

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdNam);
        rdNam.setText("Nam");

        buttonGroup1.add(rdNu);
        rdNu.setText("Nữ");
        rdNu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdNuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbHoTen)
                                    .addComponent(lbLop))
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(462, 462, 462))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(lbNgaySinh)
                                .addGap(18, 18, 18)
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbSDT)))
                        .addGap(39, 39, 39))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(lbMaSv)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbGender)
                            .addComponent(lbAddress)
                            .addComponent(jLabel11))
                        .addGap(65, 65, 65)))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(rdNam)
                        .addGap(18, 18, 18)
                        .addComponent(rdNu))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMaSv)
                    .addComponent(lbAddress)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbHoTen)
                    .addComponent(lbGender)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rdNam)
                    .addComponent(rdNu))
                .addGap(41, 41, 41)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbLop)
                    .addComponent(jLabel11)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbNgaySinh)
                        .addComponent(lbSDT)
                        .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btCapNhat.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\Update.png")); // NOI18N
        btCapNhat.setText("     Cập Nhật");
        btCapNhat.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btCapNhat.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btCapNhat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCapNhatActionPerformed(evt);
            }
        });

        btXoa.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\delete.png")); // NOI18N
        btXoa.setText("       Xóa");
        btXoa.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btXoa.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btXoaActionPerformed(evt);
            }
        });

        btTim.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\search2.png")); // NOI18N
        btTim.setText("     Tìm");
        btTim.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btTim.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btTim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btTimActionPerformed(evt);
            }
        });

        btThemMoi.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\add.png")); // NOI18N
        btThemMoi.setText("     Thêm Mới");
        btThemMoi.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btThemMoi.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btThemMoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btThemMoiActionPerformed(evt);
            }
        });

        lbMaSV2.setText("Mã sinh viên:");

        txtSearchId.setToolTipText("");

        btHienThiAll.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\edit.png")); // NOI18N
        btHienThiAll.setText("Hiển thị tất cả");
        btHienThiAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHienThiAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btXoa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btCapNhat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btThemMoi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtSearchId))
                    .addComponent(btHienThiAll, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(lbMaSV2)
                        .addGap(0, 43, Short.MAX_VALUE))
                    .addComponent(btTim, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbMaSV2)
                .addGap(4, 4, 4)
                .addComponent(txtSearchId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btTim)
                .addGap(18, 18, 18)
                .addComponent(btHienThiAll, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btThemMoi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btCapNhat, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btXoa, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbTTinHocVien, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1311, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTTinHocVien, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1347, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(5, 5, 5)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 512, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btCapNhatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCapNhatActionPerformed
        // TODO add your handling code here:
        // ko dc update ma sv , bo qua
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String strSearchId = txtSearchId.getText();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key52"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       String strName = jTextField2.getText();
       String strId = jTextField1.getText();
       String strClassName = jComboBox1.getSelectedItem().toString();
       String strClassId = "";
        Date date = jDateChooser1.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        String strAddress = jTextField4.getText();
        String strGender = "";
        if(rdNam.isSelected()){
            strGender = "Nam";
        }else if(rdNu.isSelected()){
            strGender = "Nữ";
        }
        String strEmail = jTextField6.getText();
        String strPhone = jTextField7.getText();
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            String sql2 = "select classid from Class where classname ='"+strClassName+"'";
            ResultSet rs = stm.executeQuery(sql2);//import pk resultset
            if(rs.next()){ // neu co dl
                strClassId = rs.getString("classid");
            }
            
             if(!(testCase())){
                JOptionPane.showMessageDialog(this,bundle.getString("key53"));
            }
            if (!(testCaseUpdateTrungIdMailPhone())){
                JOptionPane.showMessageDialog(this, "Email hoặc Sđt đã bị trùng"); 
            }
            if(testCase() & testCaseUpdateTrungIdMailPhone())
            {
                  String sql ="update Students set StdName = N'"+strName+"', ClassId = '"+strClassId+"', StdBirthday = '"+strDate+"',"
                          + "StdAddress = N'"+strAddress+"',StdGender =N'"+strGender+"',StdEmail = '"+strEmail+"',StdPhone = '"+strPhone+"'\n" +
                            "where StdId= '"+strId+"'";

                   stm.executeUpdate(sql);//import pk resultset
                   showStudent();
                   JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                   resetAll();
            }
            con.close();  
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
    }//GEN-LAST:event_btCapNhatActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        // TODO add your handling code here:
        int a = jTable2.getSelectedRow();
        String strId = jTable2.getValueAt(a, 0).toString();
        jTextField1.setText(strId);
        
        String strName = jTable2.getValueAt(a, 1).toString();
         jTextField2.setText(strName);
         
        String strClassId = jTable2.getValueAt(a, 2).toString();
        String strClassName = "";
        
            try {
               //2. Nap driver tuong ung voi database tren SQL Server:
               Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

               //3. Ket noi den database
               String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                       + "databaseName=Doan1123new5;user=sa;password=123456";
               Connection con = DriverManager.getConnection(connectionUrl);

               if (con != null) {
                   System.out.println("Connection is successful.");
               } else {
                   System.out.println("Connection is failed. Please try again.");
               }   

               Statement stm = con.createStatement();
               String sql= "select Classname from class where classid = '"+strClassId+"'";
               ResultSet rs = stm.executeQuery(sql);//import pk resultset
               // xoa du lieu truoc khi tim :


           if(rs.next()){   
               strClassName = rs.getString("ClassName");
           }

               con.close();

           } catch (ClassNotFoundException | SQLException ex) {
               ex.printStackTrace();
           }
       jComboBox1.setSelectedItem(strClassName);
        
        
        String strBirthday = jTable2.getValueAt(a, 3).toString();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = formatter.parse(strBirthday);
            jDateChooser1.setDate(date);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        
        String strAdd = jTable2.getValueAt(a, 4).toString();
        jTextField4.setText(strAdd);
        
        String strGender = jTable2.getValueAt(a, 5).toString();
        if(strGender.equals("Nam") | strGender.equals("nam")){
            rdNam.setSelected(true);
            rdNu.setSelected(false);
        }else{
            rdNu.setSelected(true);
            rdNam.setSelected(false);
        }
        
        String strEmail = jTable2.getValueAt(a, 6).toString();
        jTextField6.setText(strEmail);
        String strPhone = jTable2.getValueAt(a, 7).toString();
        jTextField7.setText(strPhone);
        System.out.println("Bạn vừa chọn sv có mã sv là : " +strId);

               
    }//GEN-LAST:event_jTable2MouseClicked

    private void btThemMoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btThemMoiActionPerformed
        // TODO add your handling code here:
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key12")); 
        head1.add(bundle.getString("key13"));
        head1.add(bundle.getString("key52"));
        head1.add(bundle.getString("key44"));
        head1.add(bundle.getString("key45"));
        head1.add(bundle.getString("key46"));
        head1.add("Email");
        head1.add(bundle.getString("key49"));
       String strName = jTextField2.getText();
       String strId = jTextField1.getText();
       String strClassName = jComboBox1.getSelectedItem().toString();
       String strClassId = "";
        Date date = jDateChooser1.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date);
        String strAddress = jTextField4.getText();
        String strGender = "";
        if(rdNam.isSelected()){
            strGender = "Nam";
        }else if(rdNu.isSelected()){
            strGender = "Nữ";
        }
        String strEmail = jTextField6.getText();
        String strPhone = jTextField7.getText();

            try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            String sql2 = "select classid from Class where classname ='"+strClassName+"'";
            ResultSet rs = stm.executeQuery(sql2);//import pk resultset
            if(rs.next()){ // neu co dl
                strClassId = rs.getString("classid");
            }
            if(!(testCase())){
                JOptionPane.showMessageDialog(this,bundle.getString("key53"));
            }
            if (!(testCaseAddTrungIdMailPhone())){
                JOptionPane.showMessageDialog(this, bundle.getString("key54"));
            }
            if(testCase() & testCaseAddTrungIdMailPhone()){
                  String sql ="insert into Students(StdId,StdName,ClassId ,StdBirthday,StdAddress,StdGender,StdEmail,StdPhone) \n" +
                  "values ('"+strId+"',N'"+strName+"','"+strClassId+"','"+strDate+"',N'"+strAddress+"',N'"+strGender+"','"+strEmail+"','"+strPhone+"')";

                   stm.executeUpdate(sql);//import pk resultset
                  System.out.println("ok");
                   showStudent();
                   JOptionPane.showMessageDialog(this, bundle.getString("key42"));
                   resetAll();
            
            
            con.close();  
            }
            }
            catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
            
    }//GEN-LAST:event_btThemMoiActionPerformed

    private void rdNuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdNuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdNuActionPerformed

    private void btTimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btTimActionPerformed
        // TODO add your handling code here:
        showStudentTrenTableTheoId();
        
        
    }//GEN-LAST:event_btTimActionPerformed

    private void btHienThiAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHienThiAllActionPerformed
        // TODO add your handling code here:
        showStudent();
        resetAll();
    }//GEN-LAST:event_btHienThiAllActionPerformed

    private void btXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btXoaActionPerformed
        // TODO add your handling code here:
         ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        String strId = jTextField1.getText();
        String strName = jTextField2.getText();
        int b = JOptionPane.showConfirmDialog(this, 
                bundle.getString("key55"),
                "",JOptionPane.YES_NO_OPTION);
        if(b == 0){
                try {
                //2. Nap driver tuong ung voi database tren SQL Server:
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                //3. Ket noi den database
                String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                        + "databaseName=Doan1123new5;user=sa;password=123456";
                Connection con = DriverManager.getConnection(connectionUrl);

                if (con != null) {
                    System.out.println("Connection is successful.");
                } else {
                    System.out.println("Connection is failed. Please try again.");
                }                

                //4. Tao va thuc hien cau lenh SQL:
                Statement stm = con.createStatement();           

                String sql = "exec XoaSv '"+strId+"' ";
                stm.executeUpdate(sql); 
                con.close();
                JOptionPane.showMessageDialog(this,  bundle.getString("key42"));
                resetAll();
                showStudent();
                } catch (ClassNotFoundException | SQLException ex) {
                    ex.printStackTrace();
                }
        }else{
            resetAll();
        }
    }//GEN-LAST:event_btXoaActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCapNhat;
    private javax.swing.JButton btHienThiAll;
    private javax.swing.JButton btThemMoi;
    private javax.swing.JButton btTim;
    private javax.swing.JButton btXoa;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> jComboBox1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JLabel lbAddress;
    private javax.swing.JLabel lbGender;
    private javax.swing.JLabel lbHoTen;
    private javax.swing.JLabel lbLop;
    private javax.swing.JLabel lbMaSV2;
    private javax.swing.JLabel lbMaSv;
    private javax.swing.JLabel lbNgaySinh;
    private javax.swing.JLabel lbSDT;
    private javax.swing.JLabel lbTTinHocVien;
    private javax.swing.JRadioButton rdNam;
    private javax.swing.JRadioButton rdNu;
    private javax.swing.JTextField txtSearchId;
    // End of variables declaration//GEN-END:variables
}
