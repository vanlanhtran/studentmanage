/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGIN;


import static LOGIN.LOGIN.locale;
import static LOGIN.TeacherLOGIN.MAGv;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.ResourceBundle;
/**
 *
 * @author PC
 */
public class Taikhoancuagiaovie extends javax.swing.JFrame {
    
    /**
     * Creates new form Taikhoancuagiaovie
     */
    public Taikhoancuagiaovie() {
        initComponents();
        chuyenngu();
        ShowTime();
    }
    public void ShowTime(){
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL,locale);
        String date = df.format(new Date());
        jLabel1.setText(date);
    }
    public void chuyenngu(){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        lbUser.setText(bundle.getString("key28"));
        lbHomNay.setText(bundle.getString("key29"));
        lbDanhChoGv.setText(bundle.getString("key30"));
        lbMaBangDiem.setText(bundle.getString("key31"));
        lbMaSv.setText(bundle.getString("key32"));
        lbMaSv2.setText(bundle.getString("key32"));
        lbMaMon.setText(bundle.getString("key33"));
        lbQuaMon.setText(bundle.getString("key34"));
        lbTrungBinhMon.setText(bundle.getString("key35"));
        lbLythuyet.setText(bundle.getString("key36"));
        lbThucHanh.setText(bundle.getString("key37"));
        btTim.setText(bundle.getString("key38"));
        btHienThiAll.setText(bundle.getString("key39"));
        btCapNhat.setText(bundle.getString("key40"));
        jLabel13.setText(bundle.getString("key99"));
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key9"));
        head1.add(bundle.getString("key10"));
        head1.add(bundle.getString("key11"));
        jTable1.setModel(new DefaultTableModel(data1, head1));
        btThoat.setText(bundle.getString("key80"));
        this.setTitle(bundle.getString("key133"));
    }
    public void ganDl(String s){
        //lbMaGv.setText(s);
         try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            String sql ="select TchName from Teachers where TchId = '"+s+"'";
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            if(rs.next()){     
                   lbMaGv.setText(rs.getString("TchName"));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        } 
    }
    public void showAllTrenTable(String s){
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        //String strMagv = ma();
        Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key31")); 
        head1.add(bundle.getString("key32"));
        head1.add(bundle.getString("key33"));
        head1.add(bundle.getString("key37"));
        head1.add(bundle.getString("key36"));
        head1.add(bundle.getString("key34"));
        head1.add(bundle.getString("key35"));       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            Statement stm = con.createStatement();
            String sql ="select * from marks where ModId in (select ModId from Modules where TchId = '"+s+"')";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
            while(rs.next()){     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("MarkId"));
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("ModId"));
                    cus1.addElement(rs.getString("PracticeMark"));
                    cus1.addElement(rs.getString("TheoryMark"));
                    cus1.addElement(rs.getString("pass"));
                    cus1.addElement(rs.getString("average"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            }       
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }   
    }
    public void resetAll(){
        txtLythuyet.setText("");
        txtMaBangDiem.setText("");
        txtMaMonHoc.setText("");
        txtThucHanh.setText("");
        txtMaSinhVien.setText("");
        txtQuaMon.setText("");
        txtTrungBinhMon.setText("");
    }
    public boolean testMark(){
        boolean test = false;
         try{
            float a = Float.parseFloat(txtLythuyet.getText());
            float b = Float.parseFloat(txtThucHanh.getText());
                if(((a >= 0) & (a <= 10)) & ((b >= 0) & (b <= 10))){
                    test = true;
                }
        }catch(Exception ex){
          
            System.out.println("K phai so");
        }
        return test;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        lbUser = new javax.swing.JLabel();
        lbHomNay = new javax.swing.JLabel();
        lbMaGv = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel10 = new javax.swing.JPanel();
        lbMaBangDiem = new javax.swing.JLabel();
        lbMaSv = new javax.swing.JLabel();
        lbMaMon = new javax.swing.JLabel();
        lbLythuyet = new javax.swing.JLabel();
        lbThucHanh = new javax.swing.JLabel();
        txtMaBangDiem = new javax.swing.JTextField();
        txtMaSinhVien = new javax.swing.JTextField();
        txtLythuyet = new javax.swing.JTextField();
        txtThucHanh = new javax.swing.JTextField();
        txtMaMonHoc = new javax.swing.JTextField();
        lbTrungBinhMon = new javax.swing.JLabel();
        lbQuaMon = new javax.swing.JLabel();
        txtQuaMon = new javax.swing.JTextField();
        txtTrungBinhMon = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        btCapNhat = new javax.swing.JButton();
        btTim = new javax.swing.JButton();
        txtSearchMaSv = new javax.swing.JTextField();
        lbMaSv2 = new javax.swing.JLabel();
        btHienThiAll = new javax.swing.JButton();
        btThoat = new javax.swing.JButton();
        lbDanhChoGv = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbUser.setText("Người Dùng:");

        lbHomNay.setText("Hôm Nay:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lbUser)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbMaGv, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE))
                            .addComponent(lbHomNay))
                        .addGap(22, 22, 22))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbUser)
                    .addComponent(lbMaGv, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lbHomNay)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 0, 51));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Grade Management For HKL Training Center");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1202, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Mã ", "Tên", "Thông Tin"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbMaBangDiem.setText("Mã Bảng Điểm");

        lbMaSv.setText("Mã Sinh Viên ");

        lbMaMon.setText("Mã Môn Học");

        lbLythuyet.setText("Điểm Lý Thuyết ");

        lbThucHanh.setText("Điểm Thực Hành");

        txtMaBangDiem.setEditable(false);
        txtMaBangDiem.setText(" ");

        txtMaSinhVien.setEditable(false);
        txtMaSinhVien.setText(" ");

        txtLythuyet.setText(" ");
        txtLythuyet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLythuyetActionPerformed(evt);
            }
        });

        txtThucHanh.setText(" ");

        txtMaMonHoc.setEditable(false);
        txtMaMonHoc.setText(" ");

        lbTrungBinhMon.setText("Trung Bình Môn");

        lbQuaMon.setText("Qua Môn");

        txtQuaMon.setEditable(false);

        txtTrungBinhMon.setEditable(false);
        txtTrungBinhMon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTrungBinhMonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbMaBangDiem)
                    .addComponent(lbMaSv)
                    .addComponent(lbMaMon))
                .addGap(30, 30, 30)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMaSinhVien, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                    .addComponent(txtMaBangDiem)
                    .addComponent(txtMaMonHoc))
                .addGap(160, 160, 160)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbQuaMon)
                    .addComponent(lbTrungBinhMon))
                .addGap(45, 45, 45)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTrungBinhMon, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtQuaMon, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbThucHanh, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbLythuyet, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtLythuyet, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtThucHanh, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMaBangDiem)
                    .addComponent(txtMaBangDiem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbQuaMon)
                    .addComponent(txtQuaMon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbThucHanh)
                    .addComponent(txtThucHanh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMaSv)
                    .addComponent(txtMaSinhVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTrungBinhMon)
                    .addComponent(txtTrungBinhMon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbLythuyet)
                    .addComponent(txtLythuyet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMaMon)
                    .addComponent(txtMaMonHoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btCapNhat.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\Update.png")); // NOI18N
        btCapNhat.setText("Cập Nhật");
        btCapNhat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCapNhatActionPerformed(evt);
            }
        });

        btTim.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\search2.png")); // NOI18N
        btTim.setText("Tìm kiếm");
        btTim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btTimActionPerformed(evt);
            }
        });

        lbMaSv2.setText("Mã Sinh Viên:");

        btHienThiAll.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\edit.png")); // NOI18N
        btHienThiAll.setText("Hiển Thị Tất Cả");
        btHienThiAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btHienThiAllActionPerformed(evt);
            }
        });

        btThoat.setIcon(new javax.swing.ImageIcon("E:\\batch123\\java3\\DoAnHKII\\src\\Icon\\thoat.png")); // NOI18N
        btThoat.setText("Thoát");
        btThoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btThoatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btCapNhat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btHienThiAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btThoat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(btTim)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbMaSv2)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(txtSearchMaSv, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addComponent(lbMaSv2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtSearchMaSv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btTim)
                .addGap(18, 18, 18)
                .addComponent(btHienThiAll, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btCapNhat, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btThoat, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        lbDanhChoGv.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbDanhChoGv.setForeground(new java.awt.Color(0, 51, 204));
        lbDanhChoGv.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbDanhChoGv.setText("Dành Cho Giáo Viên");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(lbDanhChoGv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel9Layout.createSequentialGroup()
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbDanhChoGv)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtLythuyetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLythuyetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLythuyetActionPerformed

    private void btTimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btTimActionPerformed
        // TODO add your handling code here:
        String strSearch = txtSearchMaSv.getText();
          ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
         Vector datanull = new Vector();
        Vector data1 = new Vector();
        Vector head1  = new Vector();
        head1.add(bundle.getString("key31")); 
        head1.add(bundle.getString("key32"));
        head1.add(bundle.getString("key33"));
        head1.add(bundle.getString("key37"));
        head1.add(bundle.getString("key36"));
        head1.add(bundle.getString("key34"));
        head1.add(bundle.getString("key35"));  
       
        try {
            //2. Nap driver tuong ung voi database tren SQL Server:
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            
            //3. Ket noi den selectdatabase
            String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                    + "databaseName=Doan1123new5;user=sa;password=123456";
            Connection con = DriverManager.getConnection(connectionUrl);
            
            if (con != null) {
                System.out.println("Connection is successful.");
            } else {
                System.out.println("Connection is failed. Please try again.");
            }
            
            
            // hien thi du lieu tren table:
            //1. get dât from table in db --> co dc rs : chua all data của bảng
            Statement stm = con.createStatement();
          String sql ="select * from Marks where ModId in (select ModId from Modules where TchId = '"+MAGv+"') and StdId = '"+strSearch+"'";
           jTable1.setModel(new DefaultTableModel(datanull, head1)); 
            ResultSet rs = stm.executeQuery(sql);//import pk resultset
            // xoa du lieu truoc khi tim :
            //jTable2.setModel(new DefaultTableModel(datanull, head1));
         
          if(rs.next()){
              do{     
                    Vector cus1 = new Vector();
                    cus1.addElement(rs.getString("MarkId"));
                    cus1.addElement(rs.getString("StdId"));
                    cus1.addElement(rs.getString("ModId"));
                    cus1.addElement(rs.getString("PracticeMark"));
                    cus1.addElement(rs.getString("TheoryMark"));
                    cus1.addElement(rs.getString("pass"));
                    cus1.addElement(rs.getString("average"));
                    data1.add(cus1);
                     jTable1.setModel(new DefaultTableModel(data1, head1));
            } while(rs.next());    
          } else{
              JOptionPane.showMessageDialog(this, bundle.getString("key26"));
          }
           resetAll();
   
            con.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        
    }//GEN-LAST:event_btTimActionPerformed

    private void btHienThiAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btHienThiAllActionPerformed
        // TODO add your handling code here:
        showAllTrenTable(MAGv);
        resetAll();
    }//GEN-LAST:event_btHienThiAllActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        int a = jTable1.getSelectedRow();
        txtMaBangDiem.setText(jTable1.getValueAt(a,0).toString());
        txtMaSinhVien.setText(jTable1.getValueAt(a,1).toString());
        txtMaMonHoc.setText(jTable1.getValueAt(a,2).toString());
        txtThucHanh.setText(jTable1.getValueAt(a,3).toString());
        txtLythuyet.setText(jTable1.getValueAt(a,4).toString());
        txtQuaMon.setText(jTable1.getValueAt(a,5).toString());
        txtTrungBinhMon.setText(jTable1.getValueAt(a,6).toString());
    }//GEN-LAST:event_jTable1MouseClicked

    private void txtTrungBinhMonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTrungBinhMonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTrungBinhMonActionPerformed

    private void btCapNhatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCapNhatActionPerformed
        // TODO add your handling code here:
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        int a = Integer.parseInt(txtMaBangDiem.getText());
        if(!testMark()){
            JOptionPane.showMessageDialog(this, bundle.getString("key41"));
        }else{
                float nbTheoMark = Float.parseFloat(txtLythuyet.getText());
                float nbPracMark = Float.parseFloat(txtThucHanh.getText());
                float average = (nbPracMark + nbTheoMark)/2;
                String pass = "Có";
                if((nbPracMark < 5) | (nbTheoMark < 5)){
                    pass = "Không";
                }
                 try {
                    //2. Nap driver tuong ung voi database tren SQL Server:
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

                    //3. Ket noi den selectdatabase
                    String connectionUrl = "jdbc:sqlserver://localhost:1433;"
                            + "databaseName=Doan1123new5;user=sa;password=123456";
                    Connection con = DriverManager.getConnection(connectionUrl);

                    if (con != null) {
                        System.out.println("Connection is successful.");
                    } else {
                        System.out.println("Connection is failed. Please try again.");
                    }
                    Statement stm = con.createStatement();              
                  String sql ="exec GVupdateMark '"+a+"' ,'"+nbPracMark+"','"+nbTheoMark+"','"+pass+"','"+average+"'";

                    stm.executeUpdate(sql);//import pk resultset
                    JOptionPane.showMessageDialog(this,bundle.getString("key42"));
                    resetAll();
                     showAllTrenTable(MAGv);
                    con.close();

                } catch (ClassNotFoundException | SQLException ex) {
                    ex.printStackTrace();
                }
        }
    }//GEN-LAST:event_btCapNhatActionPerformed

    private void btThoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btThoatActionPerformed
        // TODO add your handling code here:
        ResourceBundle bundle = ResourceBundle.getBundle("dictionary.default", locale);
        int b = JOptionPane.showConfirmDialog(this, 
                bundle.getString("key98"),
                "",JOptionPane.YES_NO_OPTION);
        if(b == 0){
            this.setVisible(false);
            LOGIN a = new LOGIN();
            a.setVisible(true);
        }else{
            
        }
    }//GEN-LAST:event_btThoatActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Taikhoancuagiaovie.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Taikhoancuagiaovie.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Taikhoancuagiaovie.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Taikhoancuagiaovie.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Taikhoancuagiaovie().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCapNhat;
    private javax.swing.JButton btHienThiAll;
    private javax.swing.JButton btThoat;
    private javax.swing.JButton btTim;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lbDanhChoGv;
    private javax.swing.JLabel lbHomNay;
    private javax.swing.JLabel lbLythuyet;
    private javax.swing.JLabel lbMaBangDiem;
    private javax.swing.JLabel lbMaGv;
    private javax.swing.JLabel lbMaMon;
    private javax.swing.JLabel lbMaSv;
    private javax.swing.JLabel lbMaSv2;
    private javax.swing.JLabel lbQuaMon;
    private javax.swing.JLabel lbThucHanh;
    private javax.swing.JLabel lbTrungBinhMon;
    private javax.swing.JLabel lbUser;
    private javax.swing.JTextField txtLythuyet;
    private javax.swing.JTextField txtMaBangDiem;
    private javax.swing.JTextField txtMaMonHoc;
    private javax.swing.JTextField txtMaSinhVien;
    private javax.swing.JTextField txtQuaMon;
    private javax.swing.JTextField txtSearchMaSv;
    private javax.swing.JTextField txtThucHanh;
    private javax.swing.JTextField txtTrungBinhMon;
    // End of variables declaration//GEN-END:variables
}
